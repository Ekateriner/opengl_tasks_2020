#include <vector>
#include <queue>
#include <random>
#include <iostream>
#include <algorithm>

struct Maze {
private:
    std::vector<std::vector<int>> field;

public:
    int x_size, y_size;
    float block_weight, block_height;
    std::vector<std::pair<char, std::pair<int, int>>> objects;

    explicit Maze(int _size=10, float _block_weight=1, float _block_height=2) { //square
        x_size = _size;
        y_size = _size;
        field = std::vector<std::vector<int>>(2*x_size+1, std::vector<int>(2*y_size+1, 1));
        block_weight = _block_weight;
        block_height = _block_height;

        for(int i = 1; i < 2*x_size; i += 2) {
            for (int j = 1; j < 2 * y_size; j += 2) {
                field[i][j] = 0;
            }
        }

        maze_generator();
    }

    explicit Maze(int _x_size, int _y_size, int _block_weight=1, int _block_height=2) {
        x_size = _x_size;
        y_size = _y_size;
        field = std::vector<std::vector<int>>(2*x_size+1, std::vector<int>(2*y_size+1, 1));
        block_weight = _block_weight;
        block_height = _block_height;

        for(int i = 1; i < 2*x_size; i += 2) {
            for (int j = 1; j < 2 * y_size; j += 2) {
                field[i][j] = 0;
            }
        }

        maze_generator();
    }

    [[nodiscard]] const std::vector<std::vector<int>>& get_field() const{
        return field;
    }

    friend std::ostream& operator<<(std::ostream& os, const Maze& maze) {
        for (int i = 0; i < 2 * maze.x_size + 1; i++) {
            for (int j = 0; j < 2 * maze.y_size + 1; j++) {
                os << maze.field[i][j] << " ";
            }
            os << std::endl;
        }
        return os;
    }

private:
    void maze_generator() {
        std::queue<std::pair<int, int>> queue;
        queue.emplace(1, 1);

        std::vector<std::vector<int>> visited = field;
        int vis_count = 0;

        std::random_device rd;
        std::mt19937 gen(rd());

        do {
            while (!queue.empty()) {
                std::pair<int, int> cur = queue.front();
                queue.pop();
                if (visited[cur.first][cur.second])
                    continue;

                visited[cur.first][cur.second] = 1;
                vis_count += 1;

                std::vector<std::pair<int, int>> next;
                if (cur.first > 1 && !visited[cur.first - 2][cur.second])
                    next.emplace_back(cur.first - 2, cur.second);

                if (cur.second > 1 && !visited[cur.first][cur.second - 2])
                    next.emplace_back(cur.first, cur.second - 2);

                if (cur.first < 2 * x_size - 1 && !visited[cur.first + 2][cur.second])
                    next.emplace_back(cur.first + 2, cur.second);

                if (cur.second < 2 * y_size - 1 && !visited[cur.first][cur.second + 2])
                    next.emplace_back(cur.first, cur.second + 2);

                std::shuffle(next.begin(), next.end(), gen);
                if (next.empty())
                    continue;

                std::vector<int> distr(next.size(), 0);
                distr[0] = 1;
                for (int i = 1; i < distr.size(); i++) {
                    std::bernoulli_distribution d(0.1);
                    distr[i] = d(gen);
                }
                //std::shuffle(distr.begin(), distr.end(), gen);

                for (int i = 0; i < next.size(); i++) {
                    if (distr[i]) {
                        queue.push(next[i]);
                        field[(cur.first + next[i].first) / 2][(cur.second + next[i].second) / 2] = 0;
                    }
                }

            }

            bool fl = true;
            for(int i = 1; i < 2*x_size && fl; i += 2) {
                for (int j = 1; j < 2 * y_size && fl; j += 2) {
                    if(visited[i][j] == 0) {
                        queue.emplace(i, j);
                        fl = false;

                        std::vector<std::pair<int, int>> vis_next;
                        if (i > 1 && visited[i - 2][j])
                            vis_next.emplace_back(i - 2, j);

                        if (j > 1 && visited[i][j - 2])
                            vis_next.emplace_back(i, j - 2);

                        if (i < 2 * x_size - 1 && visited[i + 2][j])
                            vis_next.emplace_back(i + 2, j);

                        if (j < 2 * y_size - 1 && visited[i][j + 2])
                            vis_next.emplace_back(i, j + 2);

                        std::shuffle(vis_next.begin(), vis_next.end(), gen);
                        field[(i + vis_next[0].first) / 2][(j + vis_next[0].second) / 2] = 0;
                    }
                }
            }
        } while (vis_count < x_size * y_size);
    }
};

