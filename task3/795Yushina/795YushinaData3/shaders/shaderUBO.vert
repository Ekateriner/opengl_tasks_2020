/*
Преобразует координаты вершины из локальной системы координат в Clip Space.
Копирует цвет вершины из вершинного атрибута в выходную переменную color.
*/

#version 330


uniform mat4 viewMatrix;
uniform mat4 projectionMatrix;
uniform mat4 modelMatrix;

//матрица для преобразования нормалей из локальной системы координат в систему координат камеры
uniform mat3 ToCameraMatrix;

layout(location = 0) in vec3 vertexPosition;
layout(location = 1) in vec3 vertexNormal;

out vec4 color;

void main()
{
    vec3 normalCamSpace = normalize(ToCameraMatrix * vertexNormal);  //преобразование нормали в систему координат камеры
    color = vec4(normalCamSpace, 1.0) * 0.5 + 0.5;//vertexColor;

    gl_Position = projectionMatrix * viewMatrix * modelMatrix * vec4(vertexPosition, 1.0);
}
