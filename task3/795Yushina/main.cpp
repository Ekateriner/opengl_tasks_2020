//#include <iostream>
#include <vector>
#include <filesystem>
#include <glm/gtx/vector_angle.hpp>

#include "maze.cpp"
#include "camera.cpp"
#include <Application.hpp>
#include <Mesh.hpp>
#include <ShaderProgram.hpp>
#include <Texture.hpp>
#include <LightInfo.hpp>

class MazeApplication : public Application
{
public:
    Maze maze;
    std::vector<MeshPtr> _blocks;
    MeshPtr _floor;
    MeshPtr _ceil;

    LightInfo _light;
    LightInfo _sun;

    bool is_sun = true;
    //Переменные для управления положением одного источника света
//    float _lr = 10.0f;
//    float _phi = 0.0f;
//    float _theta = 0.48f;

    TexturePtr _wallTex;
    TexturePtr _wall_normalsTex;
    TexturePtr _wall_depthTex;
    TexturePtr _floorTex;
    TexturePtr _floor_normalsTex;
    TexturePtr _floor_depthTex;
    std::vector<TexturePtr> _ceilTex;

    ShaderProgramPtr _shader;

    MeshPtr _sun_marker; //Меш - маркер для источника света
    MeshPtr _light_marker; //Меш - маркер для источника света
    ShaderProgramPtr _markerShader;

    MeshPtr _person;
    ShaderProgramPtr _person_shader;

    MeshPtr _quad;
    ShaderProgramPtr _quadShader;

    GLuint _sampler;

    bool backFaceCullEnabled = true;
    bool blendEnabled = false;
    bool depthTestEnabled = true;

    GLuint _minimapId;
    unsigned int _mmWidth = 300;
    unsigned int _mmHeight = 300;

    GLuint _renderMinimapTexId;

    GLuint _portal1Id;
    GLuint _portal2Id;
    unsigned int _pWidth = 512;
    unsigned int _pHeight = 1024;
    GLuint _renderPortal1TexId;
    GLuint _renderPortal2TexId;

    TexturePtr _portalTex;

    MazeApplication(){
        maze = Maze(10, 1.0f, 3.0f);
        _mcameraMover = std::make_shared<MazeCameraMover>(float(maze.x_size - 1) * maze.block_weight,
                                                         float(maze.y_size - 1) * maze.block_weight);
        _ocameraMover = std::make_shared<OrbitCameraMover>();
        _ocameraMover->setOrientationParameters((maze.x_size + maze.y_size) * 1.5, 0.7, -0.2);

        _cameraMover = _mcameraMover;

        portals = std::make_pair(glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.0f, 0.0f, 0.0f));
    }

    void initFramebuffer_DSA(GLuint& _framebufferId, unsigned int _fbWidth, unsigned int _fbHeight, GLuint& _renderTexId) {
        //Создаем фреймбуфер
        glCreateFramebuffers(1, &_framebufferId);

        //----------------------------

        //Создаем текстуру, куда будет осуществляться рендеринг minimap
        glCreateTextures(GL_TEXTURE_2D, 1, &_renderTexId);
        glTextureStorage2D(_renderTexId, 1, GL_RGB8, _fbWidth, _fbHeight);
        glNamedFramebufferTexture(_framebufferId, GL_COLOR_ATTACHMENT0, _renderTexId, 0);

        //----------------------------

        //Создаем текстуру глубины для фреймбуфера
        GLuint depthTexture;
        glCreateTextures(GL_TEXTURE_2D, 1, &depthTexture);
        glTextureStorage2D(depthTexture, 1, GL_DEPTH_COMPONENT24, _fbWidth, _fbHeight);
        glNamedFramebufferTexture(_framebufferId, GL_DEPTH_ATTACHMENT, depthTexture, 0);

        //----------------------------

        //Указываем куда пойдут выходные переменные фрагментного шейдера.
        GLenum buffers[] = { GL_COLOR_ATTACHMENT0};
        glNamedFramebufferDrawBuffers(_framebufferId, 1, buffers);

        // Проверяем, что в фреймбуфер можно рендерить.
        if (glCheckNamedFramebufferStatus(_framebufferId, GL_DRAW_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
        {
            std::cerr << "Failed to setup framebuffer\n";
            exit(1);
        }
    }

    void initFramebuffer_noDSA(GLuint& _framebufferId, unsigned int _fbWidth, unsigned int _fbHeight, GLuint& _renderTexId)
    {
        //Создаем фреймбуфер
        glGenFramebuffers(1, &_framebufferId);
        glBindFramebuffer(GL_DRAW_FRAMEBUFFER, _framebufferId);

        //----------------------------

        //Создаем текстуру, куда будет осуществляться рендеринг
        glGenTextures(1, &_renderTexId);
        glBindTexture(GL_TEXTURE_2D, _renderTexId);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB8, _fbWidth, _fbHeight, 0, GL_RGB, GL_UNSIGNED_BYTE, 0);

        glFramebufferTexture2D(GL_DRAW_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, _renderTexId, 0);

        //----------------------------

        //Создаем буфер глубины для фреймбуфера
        GLuint depthRenderBuffer;
        glGenRenderbuffers(1, &depthRenderBuffer);
        glBindRenderbuffer(GL_RENDERBUFFER, depthRenderBuffer);
        glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, _fbWidth, _fbHeight);

        glFramebufferRenderbuffer(GL_DRAW_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderBuffer);

        //----------------------------

        //Указываем куда именно мы будем рендерить
        GLenum buffers[] = { GL_COLOR_ATTACHMENT0 };
        glDrawBuffers(1, buffers);

        if (glCheckFramebufferStatus(GL_DRAW_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
        {
            std::cerr << "Failed to setup framebuffer\n";
            exit(1);
        }

        glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
    }

    void initFramebuffer(GLuint& _framebufferId, unsigned int _fbWidth, unsigned int _fbHeight, GLuint& _renderTexId) {
        if (USE_DSA)
            initFramebuffer_DSA(_framebufferId, _fbWidth, _fbHeight, _renderTexId);
        else
            initFramebuffer_noDSA(_framebufferId, _fbWidth, _fbHeight, _renderTexId);
    }

    void makeScene() override
    {
        Application::makeScene();

        for (int i = 0; i < 2*maze.x_size + 1; i++) {
            for (int j = 0; j < 2*maze.y_size + 1; j++) {
                if (maze.get_field(i, j) == 1) {
                    _blocks.push_back(makeBlock(maze.block_weight / 2, maze.block_height));
                    _blocks.back()->setModelMatrix(glm::translate(glm::mat4(1.0f),
                                                                  glm::vec3(float(i - maze.x_size) * maze.block_weight,
                                                                            float(maze.y_size - j) * maze.block_weight,
                                                                            0.0f)));
                }
            }
        }

        _floor = makeGroundPlane(maze.block_weight * float(2 * maze.x_size + 1) / 2);
        _floor->setModelMatrix(glm::translate(glm::mat4(1.0f),glm::vec3(0.0f, 0.0f, -0.001f)));

        _ceil = makeSkyPlane(maze.block_weight * 5 * std::max(maze.x_size, maze.y_size));
        _ceil->setModelMatrix(glm::translate(glm::mat4(1.0f),glm::vec3(0.0f, 0.0f, 2 * maze.block_height)));

        _person = loadFromFile("795YushinaData3/models/bunny.obj");
        _person->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, 0.0f)));

        _sun_marker = makeSphere(1.0f);
        _light_marker = makeSphere(0.01f);

        //=========================================================
        //Инициализация шейдеров

        _shader = std::make_shared<ShaderProgram>("795YushinaData3/shaders/common.vert", "795YushinaData3/shaders/common.frag");
        _markerShader = std::make_shared<ShaderProgram>("795YushinaData3/shaders/marker.vert", "795YushinaData3/shaders/marker.frag");
        _person_shader = std::make_shared<ShaderProgram>("795YushinaData3/shaders/shaderUBO.vert", "795YushinaData3/shaders/shaderUBO.frag");
        _quadShader = std::make_shared<ShaderProgram>("795YushinaData3/shaders/quadColor.vert", "795YushinaData3/shaders/quadColor.frag");

        //=========================================================
        //Инициализация значений переменных освщения
        _light.position = _mcameraMover->get_pos();
        _light.ambient = glm::vec3(0.1, 0.1, 0.1);
        _light.diffuse = glm::vec3(1.0, 1.0, 1.0);
        _light.specular = glm::vec3(0.3, 0.3, 0.3);

        _sun.position = glm::vec3(0, 0, 10);
        _sun.ambient = glm::vec3(0.6, 0.6, 0.6);
        _sun.diffuse = glm::vec3(0.8, 0.8, 0.8);
        _sun.specular = glm::vec3(1.0, 1.0, 1.0);

        //=========================================================
        //Инициализация minimap
        _mini_map.viewMatrix = glm::lookAt(_mcameraMover->get_pos() + glm::vec3(0.001f, 0.0f, 5.5f),
                                           _mcameraMover->get_pos() - glm::vec3(0.0f, 0.0f, 1.5f),
                                           glm::vec3(0.0f, 0.0f, 1.0f));
        int width, height;
        glfwGetFramebufferSize(_window, &width, &height);
        _mini_map.projMatrix = glm::perspective(glm::radians(45.0f), (float)_mmWidth / _mmHeight, 0.1f, 100.f);

        _quad = makeScreenAlignedQuad();

        //=========================================================
        //Загрузка и создание текстур
        _wallTex = loadTexture("795YushinaData3/textures/with_normal/wall.jpg");
        _wall_normalsTex = loadTexture("795YushinaData3/textures/with_normal/wall_normal.jpg");
        _wall_depthTex = loadTexture("795YushinaData3/textures/with_normal/wall_depth.png");
        _floorTex = loadTexture("795YushinaData3/textures/with_normal/mossroad.jpg");
        _floor_normalsTex = loadTexture("795YushinaData3/textures/with_normal/mossroad_normal.jpg");
        _floor_depthTex = loadTexture("795YushinaData3/textures/with_normal/mossroad_depth.png");
//        for (const auto & entry : std::filesystem::directory_iterator("795YushinaData3/textures/video/gif_stars"))
//            _ceilTex.push_back(loadTexture(entry.path()));
        _ceilTex.push_back(loadTexture("795YushinaData3/textures/video/gif_stars/out1.png"));
        _portalTex = loadTexture("795YushinaData3/textures/bad_img/sky_space.jpg");

        //=========================================================
        //Инициализация Uniform Buffer Object

        glGenSamplers(1, &_sampler);
        glSamplerParameteri(_sampler, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glSamplerParameteri(_sampler, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glSamplerParameteri(_sampler, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glSamplerParameteri(_sampler, GL_TEXTURE_WRAP_T, GL_REPEAT);

        //=========================================================
        //Инициализация фреймбуфера для рендеринга в текстуру

        initFramebuffer(_minimapId, _mmWidth, _mmHeight, _renderMinimapTexId);
        initFramebuffer(_portal1Id, _pWidth, _pHeight, _renderPortal1TexId);
        initFramebuffer(_portal2Id, _pWidth, _pHeight, _renderPortal2TexId);
    }

    void handleKey(int key, int scancode, int action, int mods) override
    {
        Application::handleKey(key, scancode, action, mods);

        if (action == GLFW_PRESS)
        {
            if (key == GLFW_KEY_1)
            {
                backFaceCullEnabled = !backFaceCullEnabled;
            }
            else if (key == GLFW_KEY_2)
            {
                blendEnabled = !blendEnabled;
            }
            else if (key == GLFW_KEY_3)
            {
                depthTestEnabled = !depthTestEnabled;
            }
            else if (key == GLFW_KEY_M)
            {
                _cameraMover = _mcameraMover;
            }
            else if (key == GLFW_KEY_N)
            {
                _cameraMover = _ocameraMover;
            }
            else if (key == GLFW_KEY_L)
            {
                is_sun = !is_sun;
            }
            else if (key == GLFW_KEY_G)
            {
                gmini = (gmini + 1) % 3;
            }
            else if (key == GLFW_KEY_O)
            {
                //portal 1
                std::pair<glm::vec3, glm::vec3> pos_norm = get_near();
                portals.first = pos_norm.first;
                portals_norm.first = pos_norm.second;
                portals_mesh.first = makePortal(pos_norm.second, maze.block_weight, maze.block_height);
                portals_mesh.first->setModelMatrix(glm::translate(glm::mat4(1.0f),glm::vec3(pos_norm.first.x, pos_norm.first.y, 0.0f)));
            }
            else if (key == GLFW_KEY_P)
            {
                //portal 2
                std::pair<glm::vec3, glm::vec3> pos_norm = get_near();
                portals.second = pos_norm.first;
                portals_norm.second = pos_norm.second;
                portals_mesh.second = makePortal(pos_norm.second, maze.block_weight, maze.block_height);
                portals_mesh.second->setModelMatrix(glm::translate(glm::mat4(1.0f),glm::vec3(pos_norm.first.x, pos_norm.first.y, 0.0f)));
            }
        }
        if (gmini == 0) {
            _mini_map.viewMatrix = glm::lookAt(_mcameraMover->get_pos() + glm::vec3(0.001f, 0.0f, 5.5f),
                                               _mcameraMover->get_pos() - glm::vec3(0.0f, 0.0f, 1.5f),
                                               glm::vec3(0.0f, 0.0f, 1.0f));
            int width, height;
            glfwGetFramebufferSize(_window, &width, &height);
            _mini_map.projMatrix = glm::perspective(glm::radians(45.0f), (float)width / height, 0.1f, 100.f);
        }
        else if (gmini == 1) {
            _mini_map.viewMatrix = glm::lookAt(glm::vec3(0.001f, 0.0f, 25.0f),
                                               glm::vec3(0.0f, 0.0f, 0.0f),
                                               glm::vec3(0.0f, 0.0f, 1.0f));
            int width, height;
            glfwGetFramebufferSize(_window, &width, &height);
            _mini_map.projMatrix = glm::perspective(glm::radians(45.0f), (float)width / height, 0.1f, 100.f);
        }

        if (glm::length(portals.first) > 0 && glm::length(portals.second) > 0) {
            portals_cams.second.viewMatrix = glm::lookAt(portals.first + glm::vec3(0.0f, 0.0f, 1.5f),
                                                        portals.first + 5.0f * portals_norm.first + glm::vec3(0.0f, 0.0f, 1.4f),
                                                        glm::vec3(0.0f, 0.0f, 1.0f));
            int width, height;
            glfwGetFramebufferSize(_window, &width, &height);
            portals_cams.second.projMatrix = glm::perspective(glm::radians(45.0f), (float)width / height, 0.1f, 100.f);

            portals_cams.first.viewMatrix = glm::lookAt(portals.second + glm::vec3(0.0f, 0.0f, 1.5f),
                                                        portals.second + 5.0f * portals_norm.second + glm::vec3(0.0f, 0.0f, 1.4f),
                                                        glm::vec3(0.0f, 0.0f, 1.0f));
            portals_cams.first.projMatrix = glm::perspective(glm::radians(45.0f), (float)width / height, 0.1f, 100.f);
        }
    }

    void draw() override {
        //Очищаем буферы цвета и глубины от результатов рендеринга предыдущего кадра
        glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

        if (gmini != 2) {
            glBindFramebuffer(GL_DRAW_FRAMEBUFFER, _minimapId);
            glViewport(0, 0, _mmWidth, _mmHeight);

            draw_camera(_mini_map, false);
            //Отсоединяем фреймбуфер, чтобы теперь рендерить на экран
            glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
        }

        if (glm::length(portals.first) > 0 && glm::length(portals.second) > 0) {
            glBindFramebuffer(GL_DRAW_FRAMEBUFFER, _portal1Id);
            glViewport(0, 0, _pWidth, _pHeight);
            draw_camera(portals_cams.first, false);

            glBindFramebuffer(GL_DRAW_FRAMEBUFFER, _portal2Id);
            glViewport(0, 0, _pWidth, _pHeight);
            draw_camera(portals_cams.second, false);
            //Отсоединяем фреймбуфер, чтобы теперь рендерить на экран
            glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
        }

        int width, height;
        glfwGetFramebufferSize(_window, &width, &height);
        glViewport(0, 0, width, height);
        draw_camera(_camera, true);
    }

    void draw_camera(const CameraInfo& camera, bool general)
    {
        int width, height;
        glfwGetFramebufferSize(_window, &width, &height);

        //Очищаем буферы цвета и глубины от результатов рендеринга предыдущего кадра
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        //Подключаем шейдер
        _shader->use();

        //Загружаем на видеокарту значения юниформ-переменных
        _shader->setMat4Uniform("viewMatrix", camera.viewMatrix);
        _shader->setMat4Uniform("projectionMatrix", camera.projMatrix);

        if (is_sun || !general) {
            glm::vec3 sunPosCamSpace = glm::vec3(camera.viewMatrix * glm::vec4(_sun.position, 1.0));

            _shader->setVec3Uniform("light.pos", sunPosCamSpace); //копируем положение уже в системе виртуальной камеры
            _shader->setVec3Uniform("light.La", _sun.ambient);
            _shader->setVec3Uniform("light.Ld", _sun.diffuse);
            _shader->setVec3Uniform("light.Ls", _sun.specular);
            _shader->setIntUniform("is_sun", true);
        }
        else {
            _light.position = _mcameraMover->get_pos() + glm::vec3(0, 0, 0.2);
            glm::vec3 lightPosCamSpace = glm::vec3(camera.viewMatrix * glm::vec4(_light.position, 1.0));

            _shader->setVec3Uniform("light.pos", lightPosCamSpace); //копируем положение уже в системе виртуальной камеры
            _shader->setVec3Uniform("light.La", _light.ambient);
            _shader->setVec3Uniform("light.Ld", _light.diffuse);
            _shader->setVec3Uniform("light.Ls", _light.specular);
            _shader->setVec3Uniform("direction", glm::vec3(camera.viewMatrix * glm::vec4(_mcameraMover->get_direction(), 0.0)));
            _shader->setIntUniform("is_sun", false);
        }

        if (backFaceCullEnabled)
        {
            glEnable(GL_CULL_FACE);
            glFrontFace(GL_CCW);
            glCullFace(GL_BACK);
        }

        if (blendEnabled)
        {
            glEnable(GL_BLEND);
            glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        }

        if (!depthTestEnabled)
        {
            glDisable(GL_DEPTH_TEST);
        }

        glActiveTexture(GL_TEXTURE0);  //текстурный юнит 0
        glBindSampler(0, _sampler);
        _wallTex->bind();
        _shader->setIntUniform("diffuseTex", 0);

        glActiveTexture(GL_TEXTURE1); //текстурный юнит 1
        glBindSampler(1, _sampler);
        _wall_normalsTex->bind();
        _shader->setIntUniform("normalMap", 1);

        glActiveTexture(GL_TEXTURE2); //текстурный юнит 2
        glBindSampler(2, _sampler);
        _wall_depthTex->bind();
        _shader->setIntUniform("depthMap", 2);
        _shader->setIntUniform("useNormalMap", 1 * general);
        _shader->setIntUniform("useDepthMap", 1 * general);

        //Загружаем на видеокарту матрицы модели мешей и запускаем отрисовку
        for (auto & _block : _blocks) {
            _shader->setMat4Uniform("modelMatrix", _block->modelMatrix());
            _shader->setMat3Uniform("ToCameraMatrix", glm::transpose(glm::inverse(glm::mat3(camera.viewMatrix * _block->modelMatrix()))));
            _block->draw();
        }

        glActiveTexture(GL_TEXTURE0);  //текстурный юнит 0
        glBindSampler(0, _sampler);
        _floorTex->bind();
        _shader->setIntUniform("diffuseTex", 0);

        glActiveTexture(GL_TEXTURE1); //текстурный юнит 1
        glBindSampler(1, _sampler);
        _floor_normalsTex->bind();
        _shader->setIntUniform("normalMap", 1);

        glActiveTexture(GL_TEXTURE2); //текстурный юнит 2
        glBindSampler(2, _sampler);
        _floor_depthTex->bind();
        _shader->setIntUniform("depthMap", 2);

        _shader->setIntUniform("useNormalMap", 1 * general);
        _shader->setIntUniform("useDepthMap", 1 * general);

        _shader->setMat4Uniform("modelMatrix", _floor->modelMatrix());
        _shader->setMat3Uniform("ToCameraMatrix", glm::transpose(glm::inverse(glm::mat3(camera.viewMatrix * _floor->modelMatrix()))));
        _floor->draw();

        glActiveTexture(GL_TEXTURE0);  //текстурный юнит 0
        glBindSampler(0, _sampler);
        _ceilTex[sky_time / freq]->bind();
        sky_time = (sky_time + 1) % (_ceilTex.size() * freq);
        _shader->setIntUniform("diffuseTex", 0);

        _shader->setIntUniform("useNormalMap", 0);
        _shader->setIntUniform("useDepthMap", 0);

        _shader->setMat4Uniform("modelMatrix", _ceil->modelMatrix());
        _shader->setMat3Uniform("ToCameraMatrix", glm::transpose(glm::inverse(glm::mat3(camera.viewMatrix * _ceil->modelMatrix()))));
        _ceil->draw();

        _person_shader->use();
        _person_shader->setMat4Uniform("viewMatrix", camera.viewMatrix);
        _person_shader->setMat4Uniform("projectionMatrix", camera.projMatrix);
        glm::mat4 person_disp = glm::translate(glm::mat4(1.0f), (_mcameraMover->get_pos() - glm::vec3(0.0f, 0.0f, 1.5f)));
        float angle = glm::orientedAngle(glm::vec3(1.0f, 0.0f, 0.0f),
                                         glm::normalize(_mcameraMover->get_direction() * glm::vec3(1.0f, 1.0f, 0.0f)),
                                         glm::vec3(0.0, 0.0, 1.0));
        person_disp = glm::rotate(person_disp, angle, glm::vec3(0.0, 0.0, 1.0));
        person_disp = glm::scale(person_disp,glm::vec3(0.4f));
        _person->setModelMatrix(person_disp);
        _person_shader->setMat4Uniform("modelMatrix", _person->modelMatrix());
        _person_shader->setMat3Uniform("ToCameraMatrix", glm::transpose(glm::inverse(glm::mat3(camera.viewMatrix * _person->modelMatrix()))));
        _person->draw();

        if(general && glm::length(portals.first) > 0 && glm::length(portals.second) > 0) {
            //draw portals
            _shader->use();
            glActiveTexture(GL_TEXTURE0);  //текстурный юнит 0
            glBindTexture(GL_TEXTURE_2D, _renderPortal1TexId);
            glBindSampler(0, _sampler);
            _shader->setIntUniform("diffuseTex", 0);

            _shader->setIntUniform("useNormalMap", 0);
            _shader->setIntUniform("useDepthMap", 0);

            _shader->setMat4Uniform("modelMatrix", portals_mesh.first->modelMatrix());
            _shader->setMat3Uniform("ToCameraMatrix", glm::transpose(glm::inverse(glm::mat3(camera.viewMatrix * portals_mesh.first->modelMatrix()))));
            portals_mesh.first->draw();


            glActiveTexture(GL_TEXTURE0);  //текстурный юнит 0
            glBindTexture(GL_TEXTURE_2D, _renderPortal2TexId);
            glBindSampler(0, _sampler);
            _shader->setIntUniform("diffuseTex", 0);

            _shader->setIntUniform("useNormalMap", 0);
            _shader->setIntUniform("useDepthMap", 0);

            _shader->setMat4Uniform("modelMatrix", portals_mesh.second->modelMatrix());
            _shader->setMat3Uniform("ToCameraMatrix", glm::transpose(glm::inverse(glm::mat3(camera.viewMatrix * portals_mesh.second->modelMatrix()))));
            portals_mesh.second->draw();
        }
        else if(general && glm::length(portals.first) > 0) {
            _shader->use();
            glActiveTexture(GL_TEXTURE0);  //текстурный юнит 0
            glBindSampler(0, _sampler);
            _portalTex->bind();
            _shader->setIntUniform("diffuseTex", 0);

            _shader->setIntUniform("useNormalMap", 0);
            _shader->setIntUniform("useDepthMap", 0);

            _shader->setMat4Uniform("modelMatrix", portals_mesh.first->modelMatrix());
            _shader->setMat3Uniform("ToCameraMatrix", glm::transpose(glm::inverse(glm::mat3(camera.viewMatrix * portals_mesh.first->modelMatrix()))));
            portals_mesh.first->draw();
        }
        else if(general && glm::length(portals.second) > 0) {
            _shader->use();
            glActiveTexture(GL_TEXTURE0);  //текстурный юнит 0
            glBindSampler(0, _sampler);
            _portalTex->bind();
            _shader->setIntUniform("diffuseTex", 0);

            _shader->setIntUniform("useNormalMap", 0);
            _shader->setIntUniform("useDepthMap", 0);

            _shader->setMat4Uniform("modelMatrix", portals_mesh.second->modelMatrix());
            _shader->setMat3Uniform("ToCameraMatrix", glm::transpose(glm::inverse(glm::mat3(camera.viewMatrix * portals_mesh.second->modelMatrix()))));
            portals_mesh.second->draw();
        }

        if (general && gmini != 2) //Выводим текстуру в прямоугольник на экране
        {
            _quadShader->use();

            glActiveTexture(GL_TEXTURE0);  //текстурный юнит 0
            glBindTexture(GL_TEXTURE_2D, _renderMinimapTexId);
            glBindSampler(0, _sampler);
            _quadShader->setIntUniform("tex", 0);

            glViewport(width - _mmWidth, 0, _mmWidth, _mmHeight);

            _quad->draw();
        }

        //Рисуем маркеры для всех источников света
        if (is_sun && general)
        {
            _markerShader->use();

            _markerShader->setMat4Uniform("mvpMatrix", camera.projMatrix * camera.viewMatrix * glm::translate(glm::mat4(1.0f), _sun.position));
            _markerShader->setVec4Uniform("color", glm::vec4(_light.diffuse, 1.0f));
            _sun_marker->draw();
        }
        else if (general) {
            _markerShader->use();

            _markerShader->setMat4Uniform("mvpMatrix", camera.projMatrix * camera.viewMatrix * glm::translate(glm::mat4(1.0f), _light.position));
            _markerShader->setVec4Uniform("color", glm::vec4(_light.diffuse, 1.0f));
            _light_marker->draw();
        }

        if (!depthTestEnabled)
        {
            glEnable(GL_DEPTH_TEST);
        }

        if (blendEnabled)
        {
            glDisable(GL_BLEND);
        }

        if (backFaceCullEnabled)
        {
            glDisable(GL_CULL_FACE);
        }

        //Отсоединяем сэмплер и шейдерную программу
        glBindSampler(0, 0);
        glUseProgram(0);
    }

protected:
    std::shared_ptr<MazeCameraMover> _mcameraMover;
    std::shared_ptr<OrbitCameraMover> _ocameraMover;
    int sky_time = 0;
    const int freq = 100;

    int gmini = 0;
    CameraInfo _mini_map;

    std::pair<glm::vec3, glm::vec3> portals;
    std::pair<glm::vec3, glm::vec3> portals_norm;
    std::pair<MeshPtr, MeshPtr> portals_mesh;
    std::pair<CameraInfo, CameraInfo> portals_cams;

    std::pair<glm::vec3, glm::vec3> get_near() {
        glm::vec3 current = _mcameraMover->get_pos();
        // (_mcameraMover->get_pos().x / maze.block_weight + maze.x_size)
        glm::vec3 dir = glm::normalize(_mcameraMover->get_direction());

        if (dir.x == 0 && dir.y == 0) {
            return std::make_pair(glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.0f, 0.0f, 0.0f));
        }

        int cur_x = int(std::round(current.x / maze.block_weight)) + maze.x_size;
        int cur_y = maze.y_size - int(std::round(current.y / maze.block_weight));
        bool last_x = true;
        while (!maze.get_field(cur_x, cur_y) && current.z < maze.block_height && current.z > 0.0f) {
            if (dir.y == 0) {
                float x_dist = 0;
                if (dir.x > 0 && current.x > 0) {
                    x_dist = (std::trunc(std::abs(current.x - maze.block_weight / 2) / maze.block_weight) + 1) *
                             maze.block_weight - std::abs(current.x - maze.block_weight / 2);
                } else {
                    x_dist = std::abs(current.x - maze.block_weight / 2) -
                             (std::trunc(std::abs(current.x - maze.block_weight / 2) / maze.block_weight)) *
                             maze.block_weight;
                }

                float alpha_x = x_dist / std::abs(dir.x) + 0.001;

                current += alpha_x * dir;
                last_x = true;
            } else if (dir.x == 0) {
                float y_dist = 0;
                if (dir.y > 0 && current.y > 0) {
                    y_dist = (std::trunc(std::abs(current.y - maze.block_weight / 2) / maze.block_weight) + 1) *
                             maze.block_weight - std::abs(current.y - maze.block_weight / 2);
                } else {
                    y_dist = std::abs(current.y - maze.block_weight / 2) -
                             (std::trunc(std::abs(current.y - maze.block_weight / 2) / maze.block_weight)) *
                             maze.block_weight;
                }

                float alpha_y = y_dist / std::abs(dir.y) + 0.001;
                current += alpha_y * dir;
                last_x = false;
            }
            else {
                float x_dist = 0;
                if (dir.x > 0 && current.x > 0) {
                    x_dist = (std::trunc(std::abs(current.x - maze.block_weight / 2) / maze.block_weight) + 1) *
                             maze.block_weight - std::abs(current.x - maze.block_weight / 2);
                } else {
                    x_dist = std::abs(current.x - maze.block_weight / 2) -
                             (std::trunc(std::abs(current.x - maze.block_weight / 2) / maze.block_weight)) *
                             maze.block_weight;
                }

                float y_dist = 0;
                if (dir.y > 0 && current.y > 0) {
                    y_dist = (std::trunc(std::abs(current.y - maze.block_weight / 2) / maze.block_weight) + 1) *
                             maze.block_weight - std::abs(current.y - maze.block_weight / 2);
                } else {
                    y_dist = std::abs(current.y - maze.block_weight / 2) -
                             (std::trunc(std::abs(current.y - maze.block_weight / 2) / maze.block_weight)) *
                             maze.block_weight;
                }

                float alpha_x = x_dist / std::abs(dir.x) + 0.001;
                float alpha_y = y_dist / std::abs(dir.y) + 0.001;

                if (alpha_x < alpha_y) {
                    current += alpha_x * dir;
                    last_x = true;
                } else {
                    current += alpha_y * dir;
                    last_x = false;
                }
            }

            cur_x = int(std::round(current.x / maze.block_weight)) + maze.x_size;
            cur_y = maze.y_size - int(std::round(current.y / maze.block_weight));
        }

        if (maze.block_height < current.z || current.z < 0) {
            return std::make_pair(glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.0f, 0.0f, 0.0f));
        }

        glm::vec3 block = glm::vec3(float(cur_x - maze.x_size) * maze.block_weight,
                                    float(maze.y_size - cur_y) * maze.block_weight,
                                    0.0f);

        float disp = 0.01;
        if (dir.x >= 0 && dir.y >= 0) {
            if (last_x){
                return std::make_pair(block - glm::vec3(maze.block_weight / 2  + disp, 0.0f, 0.0f),
                                      glm::vec3(-1.0f, 0.0f, 0.0f));
            }
            else {
                return std::make_pair(block - glm::vec3(0.0f, maze.block_weight / 2  + disp, 0.0f),
                                      glm::vec3(0.0f, -1.0f, 0.0f));
            }
        }
        else if (dir.x >= 0 && dir.y <= 0) {
            if (last_x){
                return std::make_pair(block - glm::vec3(maze.block_weight / 2  + disp, 0.0f, 0.0f),
                                      glm::vec3(-1.0f, 0.0f, 0.0f));
            }
            else {
                return std::make_pair(block + glm::vec3(0.0f, maze.block_weight / 2  + disp, 0.0f),
                                      glm::vec3(0.0f, 1.0f, 0.0f));
            }
        }
        else if (dir.x <= 0 && dir.y >= 0) {
            if (last_x){
                return std::make_pair(block + glm::vec3(maze.block_weight / 2  + disp, 0.0f, 0.0f),
                                      glm::vec3(1.0f, 0.0f, 0.0f));
            }
            else {
                return std::make_pair(block - glm::vec3(0.0f, maze.block_weight / 2  + disp, 0.0f),
                                      glm::vec3(0.0f, -1.0f, 0.0f));
            }
        }
        else if (dir.x <= 0 && dir.y <= 0) {
            if (last_x){
                return std::make_pair(block + glm::vec3(maze.block_weight / 2  + disp, 0.0f, 0.0f),
                                      glm::vec3(1.0f, 0.0f, 0.0f));
            }
            else {
                return std::make_pair(block + glm::vec3(0.0f, maze.block_weight / 2  + disp, 0.0f),
                                      glm::vec3(0.0f, 1.0f, 0.0f));
            }
        }
    }
};

int main()
{
    MazeApplication app;
    app.start();

    return 0;
}