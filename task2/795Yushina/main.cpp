//#include <iostream>
#include <vector>
#include <filesystem>

#include "maze.cpp"
#include "camera.cpp"
#include <Application.hpp>
#include <Mesh.hpp>
#include <ShaderProgram.hpp>
#include <Texture.hpp>
#include <LightInfo.hpp>


class MazeApplication : public Application
{
public:
    Maze maze;
    std::vector<MeshPtr> _blocks;
    MeshPtr _floor;
    MeshPtr _ceil;

    LightInfo _light;
    LightInfo _sun;

    bool is_sun = true;
    //Переменные для управления положением одного источника света
//    float _lr = 10.0f;
//    float _phi = 0.0f;
//    float _theta = 0.48f;

    TexturePtr _wallTex;
    TexturePtr _wall_normalsTex;
    TexturePtr _wall_depthTex;
    TexturePtr _floorTex;
    TexturePtr _floor_normalsTex;
    TexturePtr _floor_depthTex;
    std::vector<TexturePtr> _ceilTex;

    ShaderProgramPtr _shader;

    MeshPtr _sun_marker; //Меш - маркер для источника света
    MeshPtr _light_marker; //Меш - маркер для источника света
    ShaderProgramPtr _markerShader;

    GLuint _sampler;

    bool backFaceCullEnabled = true;
    bool blendEnabled = false;
    bool depthTestEnabled = true;

    MazeApplication(){
        maze = Maze(10, 1.0f, 3.0f);
        _mcameraMover = std::make_shared<MazeCameraMover>(float(maze.x_size - 1) * maze.block_weight,
                                                         float(maze.y_size - 1) * maze.block_weight);
        _ocameraMover = std::make_shared<OrbitCameraMover>();
        _ocameraMover->setOrientationParameters((maze.x_size + maze.y_size) * 1.5, 0.7, -0.2);

        _cameraMover = _mcameraMover;
    }

    void makeScene() override
    {
        Application::makeScene();

        for (int i = 0; i < 2*maze.x_size + 1; i++) {
            for (int j = 0; j < 2*maze.y_size + 1; j++) {
                if (maze.get_field()[i][j] == 1) {
                    _blocks.push_back(makeBlock(maze.block_weight / 2, maze.block_height));
                    _blocks.back()->setModelMatrix(glm::translate(glm::mat4(1.0f),
                                                                  glm::vec3(float(i - maze.x_size) * maze.block_weight,
                                                                            float(maze.y_size - j) * maze.block_weight,
                                                                            0.0f)));
                }
            }
        }

        _floor = makeGroundPlane(maze.block_weight * float(2 * maze.x_size + 1) / 2);
        _floor->setModelMatrix(glm::translate(glm::mat4(1.0f),glm::vec3(0.0f, 0.0f, -0.001f)));

        _ceil = makeSkyPlane(maze.block_weight * 4 * std::max(maze.x_size, maze.y_size));
        _ceil->setModelMatrix(glm::translate(glm::mat4(1.0f),glm::vec3(0.0f, 0.0f, 2 * maze.block_height)));

        _sun_marker = makeSphere(1.0f);
        _light_marker = makeSphere(0.01f);

        //=========================================================
        //Инициализация шейдеров

        _shader = std::make_shared<ShaderProgram>("795YushinaData2/shaders/common.vert", "795YushinaData2/shaders/common.frag");
        _markerShader = std::make_shared<ShaderProgram>("795YushinaData2/shaders/marker.vert", "795YushinaData2/shaders/marker.frag");

        //=========================================================
        //Инициализация значений переменных освщения
        _light.position = _mcameraMover->get_pos();
        _light.ambient = glm::vec3(0.1, 0.1, 0.1);
        _light.diffuse = glm::vec3(1.0, 1.0, 1.0);
        _light.specular = glm::vec3(0.3, 0.3, 0.3);

        _sun.position = glm::vec3(0, 0, 10);
        _sun.ambient = glm::vec3(0.3, 0.3, 0.3);
        _sun.diffuse = glm::vec3(0.8, 0.8, 0.8);
        _sun.specular = glm::vec3(1.0, 1.0, 1.0);

        //=========================================================
        //Загрузка и создание текстур
        _wallTex = loadTexture("795YushinaData2/textures/with_normal/wall.jpg");
        _wall_normalsTex = loadTexture("795YushinaData2/textures/with_normal/wall_normal.jpg");
        _wall_depthTex = loadTexture("795YushinaData2/textures/with_normal/wall_depth.png");
        _floorTex = loadTexture("795YushinaData2/textures/with_normal/mossroad.jpg");
        _floor_normalsTex = loadTexture("795YushinaData2/textures/with_normal/mossroad_normal.jpg");
        _floor_depthTex = loadTexture("795YushinaData2/textures/with_normal/mossroad_depth.png");
        for (const auto & entry : std::filesystem::directory_iterator("795YushinaData2/textures/video/gif_stars"))
            _ceilTex.push_back(loadTexture(entry.path()));

        //=========================================================
        //Инициализация Uniform Buffer Object

        glGenSamplers(1, &_sampler);
        glSamplerParameteri(_sampler, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glSamplerParameteri(_sampler, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glSamplerParameteri(_sampler, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glSamplerParameteri(_sampler, GL_TEXTURE_WRAP_T, GL_REPEAT);
    }

//    void updateGUI() override
//    {
//        Application::updateGUI();
//
//        ImGui::SetNextWindowPos(ImVec2(0, 0), ImGuiSetCond_FirstUseEver);
//        if (ImGui::Begin("MIPT OpenGL Sample", NULL, ImGuiWindowFlags_AlwaysAutoResize))
//        {
//            ImGui::Text("FPS %.1f", ImGui::GetIO().Framerate);
//
//            if (ImGui::CollapsingHeader("Light"))
//            {
//                ImGui::ColorEdit3("ambient", glm::value_ptr(_light.ambient));
//                ImGui::ColorEdit3("diffuse", glm::value_ptr(_light.diffuse));
//                ImGui::ColorEdit3("specular", glm::value_ptr(_light.specular));
//
//                ImGui::SliderFloat("radius", &_lr, 0.1f, 10.0f);
//                ImGui::SliderFloat("phi", &_phi, 0.0f, 2.0f * glm::pi<float>());
//                ImGui::SliderFloat("theta", &_theta, 0.0f, glm::pi<float>());
//            }
//
//            ImGui::Checkbox("Back face culling", &backFaceCullEnabled);
//            ImGui::Checkbox("Blending", &blendEnabled);
//            ImGui::Checkbox("Depth test", &depthTestEnabled);
//        }
//        ImGui::End();
//    }

    void handleKey(int key, int scancode, int action, int mods) override
    {
        Application::handleKey(key, scancode, action, mods);

        if (action == GLFW_PRESS)
        {
            if (key == GLFW_KEY_1)
            {
                backFaceCullEnabled = !backFaceCullEnabled;
            }
            else if (key == GLFW_KEY_2)
            {
                blendEnabled = !blendEnabled;
            }
            else if (key == GLFW_KEY_3)
            {
                depthTestEnabled = !depthTestEnabled;
            }
            else if (key == GLFW_KEY_M)
            {
                _cameraMover = _mcameraMover;
            }
            else if (key == GLFW_KEY_N)
            {
                _cameraMover = _ocameraMover;
            }
            else if (key == GLFW_KEY_L)
            {
                is_sun = !is_sun;
            }
        }
    }

    void draw() override
    {
        //Application::draw();

        //Получаем текущие размеры экрана и выставлям вьюпорт
        int width, height;
        glfwGetFramebufferSize(_window, &width, &height);

        glViewport(0, 0, width, height);

        //Очищаем буферы цвета и глубины от результатов рендеринга предыдущего кадра
        glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        //Подключаем шейдер
        _shader->use();

        //Загружаем на видеокарту значения юниформ-переменных
        _shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
        _shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);

        if (is_sun) {
            glm::vec3 sunPosCamSpace = glm::vec3(_camera.viewMatrix * glm::vec4(_sun.position, 1.0));

            _shader->setVec3Uniform("light.pos", sunPosCamSpace); //копируем положение уже в системе виртуальной камеры
            _shader->setVec3Uniform("light.La", _sun.ambient);
            _shader->setVec3Uniform("light.Ld", _sun.diffuse);
            _shader->setVec3Uniform("light.Ls", _sun.specular);
            _shader->setIntUniform("is_sun", true);
        }
        else {
            _light.position = _mcameraMover->get_pos() + glm::vec3(0, 0, 0.2);
            glm::vec3 lightPosCamSpace = glm::vec3(_camera.viewMatrix * glm::vec4(_light.position, 1.0));

            _shader->setVec3Uniform("light.pos", lightPosCamSpace); //копируем положение уже в системе виртуальной камеры
            _shader->setVec3Uniform("light.La", _light.ambient);
            _shader->setVec3Uniform("light.Ld", _light.diffuse);
            _shader->setVec3Uniform("light.Ls", _light.specular);
            _shader->setVec3Uniform("direction", glm::vec3(_camera.viewMatrix * glm::vec4(_mcameraMover->get_direction(), 0.0)));
            _shader->setIntUniform("is_sun", false);
        }

        if (backFaceCullEnabled)
        {
            glEnable(GL_CULL_FACE);
            glFrontFace(GL_CCW);
            glCullFace(GL_BACK);
        }

        if (blendEnabled)
        {
            glEnable(GL_BLEND);
            glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        }

        if (!depthTestEnabled)
        {
            glDisable(GL_DEPTH_TEST);
        }

        glActiveTexture(GL_TEXTURE0);  //текстурный юнит 0
        glBindSampler(0, _sampler);
        _wallTex->bind();
        _shader->setIntUniform("diffuseTex", 0);

        glActiveTexture(GL_TEXTURE1); //текстурный юнит 1
        glBindSampler(1, _sampler);
        _wall_normalsTex->bind();
        _shader->setIntUniform("normalMap", 1);

        glActiveTexture(GL_TEXTURE2); //текстурный юнит 2
        glBindSampler(2, _sampler);
        _wall_depthTex->bind();
        _shader->setIntUniform("depthMap", 2);
        _shader->setIntUniform("useNormalMap", 1);
        _shader->setIntUniform("useDepthMap", 1);

        //Загружаем на видеокарту матрицы модели мешей и запускаем отрисовку
        for (auto & _block : _blocks) {
            _shader->setMat4Uniform("modelMatrix", _block->modelMatrix());
            _shader->setMat3Uniform("ToCameraMatrix", glm::transpose(glm::inverse(glm::mat3(_camera.viewMatrix * _block->modelMatrix()))));
            _block->draw();
        }

        glActiveTexture(GL_TEXTURE0);  //текстурный юнит 0
        glBindSampler(0, _sampler);
        _floorTex->bind();
        _shader->setIntUniform("diffuseTex", 0);

        glActiveTexture(GL_TEXTURE1); //текстурный юнит 1
        glBindSampler(1, _sampler);
        _floor_normalsTex->bind();
        _shader->setIntUniform("normalMap", 1);

        glActiveTexture(GL_TEXTURE2); //текстурный юнит 2
        glBindSampler(2, _sampler);
        _floor_depthTex->bind();
        _shader->setIntUniform("depthMap", 2);

        _shader->setIntUniform("useNormalMap", 1);
        _shader->setIntUniform("useDepthMap", 1);

        _shader->setMat4Uniform("modelMatrix", _floor->modelMatrix());
        _shader->setMat3Uniform("ToCameraMatrix", glm::transpose(glm::inverse(glm::mat3(_camera.viewMatrix * _floor->modelMatrix()))));
        _floor->draw();

        glActiveTexture(GL_TEXTURE0);  //текстурный юнит 0
        glBindSampler(0, _sampler);
        _ceilTex[sky_time / freq]->bind();
        sky_time = (sky_time + 1) % (_ceilTex.size() * freq);
        _shader->setIntUniform("diffuseTex", 0);

        _shader->setIntUniform("useNormalMap", 0);
        _shader->setIntUniform("useDepthMap", 0);

        _shader->setMat4Uniform("modelMatrix", _ceil->modelMatrix());
        _shader->setMat3Uniform("ToCameraMatrix", glm::transpose(glm::inverse(glm::mat3(_camera.viewMatrix * _ceil->modelMatrix()))));
        _ceil->draw();

        if (!depthTestEnabled)
        {
            glEnable(GL_DEPTH_TEST);
        }

        if (blendEnabled)
        {
            glDisable(GL_BLEND);
        }

        if (backFaceCullEnabled)
        {
            glDisable(GL_CULL_FACE);
        }

        //Рисуем маркеры для всех источников света
        if (is_sun)
        {
            _markerShader->use();

            _markerShader->setMat4Uniform("mvpMatrix", _camera.projMatrix * _camera.viewMatrix * glm::translate(glm::mat4(1.0f), _sun.position));
            _markerShader->setVec4Uniform("color", glm::vec4(_light.diffuse, 1.0f));
            _sun_marker->draw();
        }
        else {
            _markerShader->use();

            _markerShader->setMat4Uniform("mvpMatrix", _camera.projMatrix * _camera.viewMatrix * glm::translate(glm::mat4(1.0f), _light.position));
            _markerShader->setVec4Uniform("color", glm::vec4(_light.diffuse, 1.0f));
            _light_marker->draw();
        }

        //Отсоединяем сэмплер и шейдерную программу
        glBindSampler(0, 0);
        glUseProgram(0);
    }

protected:
    std::shared_ptr<MazeCameraMover> _mcameraMover;
    std::shared_ptr<OrbitCameraMover> _ocameraMover;
    int sky_time = 0;
    const int freq = 100;
};

int main()
{
    MazeApplication app;
    app.start();

    return 0;
}