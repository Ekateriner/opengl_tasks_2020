#version 330

uniform sampler2D diffuseTex;
uniform sampler2D normalMap;
uniform sampler2D depthMap;

struct LightInfo
{
	vec3 pos; //положение источника света в системе координат ВИРТУАЛЬНОЙ КАМЕРЫ!
	vec3 La; //цвет и интенсивность окружающего света
	vec3 Ld; //цвет и интенсивность диффузного света
	vec3 Ls; //цвет и интенсивность бликового света
};
uniform LightInfo light;
uniform vec3 direction; //из мировой в систему координат камеры
uniform bool is_sun = true;
uniform int useNormalMap = 1;
uniform int useDepthMap = 0;

in vec4 posCamSpace; //координаты вершины в системе координат камеры (интерполированы между вершинами треугольника)
in vec3 normalCamSpace; //нормаль в системе координат камеры (интерполирована между вершинами треугольника)
in vec3 tangentCamSpace; //касетельная в системе координат камеры (интерполирована между вершинами треугольника)
in vec2 texCoord; //текстурные координаты (интерполирована между вершинами треугольника)

out vec4 fragColor; //выходной цвет фрагмента

const vec3 Ks = vec3(0.2, 0.2, 0.2); //Коэффициент бликового отражения
const float shininess = 100.0;

const float height_scale = 0.001;
const float height_bias = 0.0;

vec2 ParallaxMapping(vec2 texCoords, vec3 viewDir) {
	float height =  height_scale * (1 - texture(depthMap, texCoords).r) + height_bias;
	vec2 p = viewDir.xy * height / viewDir.z;
	return texCoords - p;
}

void main()
{
	vec3 normal = normalize(normalCamSpace); //нормализуем нормаль после интерполяции
	vec3 tangent = normalize(tangentCamSpace); //нормализуем нормаль после интерполяции
	vec3 bitan = cross(normal, tangent);
	mat3 TBN = mat3(tangent, bitan, normal);

	vec2 newTexCoord = texCoord;
	if (useDepthMap == 1) {
		vec3 viewDir = normalize(TBN * (-posCamSpace.xyz));
		newTexCoord = ParallaxMapping(texCoord,  viewDir);
//		if(texCoord.x > 1.0 || texCoord.y > 1.0 || texCoord.x < 0.0 || texCoord.y < 0.0)
//			discard;
	}

	vec3 diffuseColor = texture(diffuseTex, newTexCoord).rgb;
	
	vec3 lightDirCamSpace = light.pos - posCamSpace.xyz; //направление на источник света
	float distance = length(lightDirCamSpace);
	lightDirCamSpace = normalize(lightDirCamSpace); //направление на источник света

	if (useNormalMap == 1) {
		vec3 weights = texture(normalMap, texCoord).rgb * 2 - 1;
		normal =  TBN * weights;
		normal = normalize(normal);
	}

	//float NdotL = max(dot(normal, lightDirCamSpace.xyz), 0.0); //скалярное произведение (косинус)

	float NdotL = max(dot(normal, lightDirCamSpace.xyz), 0.0); //скалярное произведение (косинус)

	vec3 color = vec3(0.0, 0.0, 0.0);
	if(!is_sun) {
		float angle = max(dot(normalize(direction), normalize(posCamSpace.xyz - light.pos)), 0.9);
		color = diffuseColor * (light.La + light.Ld * NdotL * pow(10 * angle - 9, 3));
		if (NdotL > 0.0) {
			vec3 viewDirection = normalize(-posCamSpace.xyz); //направление на виртуальную камеру (она находится в точке (0.0, 0.0, 0.0))
			vec3 halfVector = normalize(lightDirCamSpace.xyz + viewDirection); //биссектриса между направлениями на камеру и на источник света

			float blinnTerm = max(dot(normal, halfVector), 0.0); //интенсивность бликового освещения по Блинну
			blinnTerm = pow(blinnTerm, shininess); //регулируем размер блика

			color += light.Ls * Ks * blinnTerm;
		}
	}
	else {
		color = diffuseColor * (light.La + light.Ld * NdotL);
		if (NdotL > 0.0) {
			vec3 viewDirection = normalize(-posCamSpace.xyz); //направление на виртуальную камеру (она находится в точке (0.0, 0.0, 0.0))
			vec3 halfVector = normalize(lightDirCamSpace.xyz + viewDirection); //биссектриса между направлениями на камеру и на источник света

			float blinnTerm = max(dot(normal, halfVector), 0.0); //интенсивность бликового освещения по Блинну
			blinnTerm = pow(blinnTerm, shininess); //регулируем размер блика

			color += light.Ls * Ks * blinnTerm;
		}
	}

	fragColor = vec4(color, 0.8);
}
