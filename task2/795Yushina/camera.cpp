#include <glm/gtx/transform.hpp>
#include <glm/gtx/quaternion.hpp>
#include <Camera.hpp>
#include <iostream>

class MazeCameraMover : public CameraMover {
public:
    MazeCameraMover(float x, float y) :
            CameraMover(),
            _pos(x, y, 1.5f)
    {
        //Нам нужно как-нибудь посчитать начальную ориентацию камеры
        _rot = glm::toQuat(glm::lookAt(_pos, glm::vec3(x, 0.0f, 1.4f), glm::vec3(0.0f, 0.0f, 1.0f)));
    }

    void handleKey(GLFWwindow* window, int key, int scancode, int action, int mods) override {}

    void handleMouseMove(GLFWwindow* window, double xpos, double ypos) override {
        int state = glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_LEFT);
        if (state == GLFW_PRESS)
        {
            double dx = xpos - _oldXPos;
            double dy = ypos - _oldYPos;

            //Добавляем небольшой поворот вверх/вниз
            glm::vec3 rightDir = glm::vec3(1.0f, 0.0f, 0.0f) * _rot;
            _rot *= glm::angleAxis(static_cast<float>(dy * 0.005), rightDir);

            //Добавляем небольшой поворов вокруг вертикальной оси
            glm::vec3 upDir(0.0f, 0.0f, 1.0f);
            _rot *= glm::angleAxis(static_cast<float>(dx * 0.005), upDir);
        }

        _oldXPos = xpos;
        _oldYPos = ypos;
    }

    void handleScroll(GLFWwindow* window, double xoffset, double yoffset) override {}

    void update(GLFWwindow* window, double dt) override {
        float speed = 2.0f;
        float rot_speed = 0.05f;

        //Получаем текущее направление "вперед" в мировой системе координат
        glm::vec3 forwDir = (glm::vec3(0.0f, 0.0f, -1.0f) * _rot) * glm::vec3(1.0f, 1.0f, 0.0f);

        //Добавляем небольшой поворов вокруг вертикальной оси
        glm::vec3 upDir(0.0f, 0.0f, 1.0f);

        //Двигаем камеру вперед/назад
        if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
        {
            _pos += forwDir * speed * static_cast<float>(dt);
        }
        if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
        {
            _pos -= forwDir * speed * static_cast<float>(dt);
        }
        if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS)
        {
            _rot *= glm::angleAxis(static_cast<float>(-rot_speed), upDir);
        }
        if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS)
        {
            _rot *= glm::angleAxis(static_cast<float>(rot_speed), upDir);
        }

        //-----------------------------------------

        //Соединяем перемещение и поворот вместе
        _camera.viewMatrix = glm::toMat4(-_rot) * glm::translate(-_pos);

        //-----------------------------------------

        int width, height;
        glfwGetFramebufferSize(window, &width, &height);

        //Обновляем матрицу проекции на случай, если размеры окна изменились
        _camera.projMatrix = glm::perspective(glm::radians(45.0f), (float)width / height, _near, _far);
    }

    glm::vec3 get_pos(){
        return _pos;
    }

    glm::vec3 get_direction(){
        return glm::vec3(0.0f, -0.2f, -1.0f) * _rot;
    }

//    void showOrientationParametersImgui() override;
//    void setOrientationParameters(double r, double phi, double theta) { _r = r; _phiAng = phi; _thetaAng = theta; }

protected:
    glm::vec3 _pos;
    glm::quat _rot;

//    double _phiAng = 0.0;
//    double _thetaAng = 0.0;
//    double _r = 5.0;

    double _oldXPos = 0.0;
    double _oldYPos = 0.0;
};
